﻿namespace AAReceivablesDetailCalculation
{
    partial class ConfirmActivityPremiseCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.m_activityPersonNameSplitter = new System.Windows.Forms.Splitter();
            this.m_activityActivityNameSplitter = new System.Windows.Forms.Splitter();
            this.m_confirmActivityPersonCountButton = new System.Windows.Forms.Button();
            this.m_activityPersonIntegerInput = new DevComponents.Editors.IntegerInput();
            this.m_resetPersonButton = new System.Windows.Forms.Button();
            this.m_confirmPersonNumberButton = new System.Windows.Forms.Button();
            this.m_activityLabel = new System.Windows.Forms.Label();
            this.m_activityCountIntegerInput = new DevComponents.Editors.IntegerInput();
            this.m_confirmActivityCountButton = new System.Windows.Forms.Button();
            this.m_resetActivityButton = new System.Windows.Forms.Button();
            this.m_confirmAllButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_activityPersonIntegerInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_activityCountIntegerInput)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "人数";
            // 
            // m_activityPersonNameSplitter
            // 
            this.m_activityPersonNameSplitter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_activityPersonNameSplitter.Location = new System.Drawing.Point(0, 0);
            this.m_activityPersonNameSplitter.Name = "m_activityPersonNameSplitter";
            this.m_activityPersonNameSplitter.Size = new System.Drawing.Size(217, 386);
            this.m_activityPersonNameSplitter.TabIndex = 1;
            this.m_activityPersonNameSplitter.TabStop = false;
            // 
            // m_activityActivityNameSplitter
            // 
            this.m_activityActivityNameSplitter.BackColor = System.Drawing.SystemColors.Control;
            this.m_activityActivityNameSplitter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_activityActivityNameSplitter.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_activityActivityNameSplitter.Location = new System.Drawing.Point(217, 0);
            this.m_activityActivityNameSplitter.Name = "m_activityActivityNameSplitter";
            this.m_activityActivityNameSplitter.Size = new System.Drawing.Size(217, 386);
            this.m_activityActivityNameSplitter.TabIndex = 2;
            this.m_activityActivityNameSplitter.TabStop = false;
            // 
            // m_confirmActivityPersonCountButton
            // 
            this.m_confirmActivityPersonCountButton.Location = new System.Drawing.Point(105, 64);
            this.m_confirmActivityPersonCountButton.Name = "m_confirmActivityPersonCountButton";
            this.m_confirmActivityPersonCountButton.Size = new System.Drawing.Size(41, 23);
            this.m_confirmActivityPersonCountButton.TabIndex = 3;
            this.m_confirmActivityPersonCountButton.Text = "OK";
            this.m_confirmActivityPersonCountButton.UseVisualStyleBackColor = true;
            this.m_confirmActivityPersonCountButton.Click += new System.EventHandler(this.OnConfirmButtonClick);
            // 
            // m_activityPersonIntegerInput
            // 
            // 
            // 
            // 
            this.m_activityPersonIntegerInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.m_activityPersonIntegerInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.m_activityPersonIntegerInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.m_activityPersonIntegerInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.m_activityPersonIntegerInput.Location = new System.Drawing.Point(45, 64);
            this.m_activityPersonIntegerInput.Name = "m_activityPersonIntegerInput";
            this.m_activityPersonIntegerInput.Size = new System.Drawing.Size(45, 23);
            this.m_activityPersonIntegerInput.TabIndex = 0;
            // 
            // m_resetPersonButton
            // 
            this.m_resetPersonButton.Location = new System.Drawing.Point(22, 351);
            this.m_resetPersonButton.Name = "m_resetPersonButton";
            this.m_resetPersonButton.Size = new System.Drawing.Size(68, 23);
            this.m_resetPersonButton.TabIndex = 4;
            this.m_resetPersonButton.Text = "重置人数";
            this.m_resetPersonButton.UseVisualStyleBackColor = true;
            this.m_resetPersonButton.Visible = false;
            this.m_resetPersonButton.Click += new System.EventHandler(this.OnResetPersonButtonClick);
            // 
            // m_confirmPersonNumberButton
            // 
            this.m_confirmPersonNumberButton.Location = new System.Drawing.Point(132, 351);
            this.m_confirmPersonNumberButton.Name = "m_confirmPersonNumberButton";
            this.m_confirmPersonNumberButton.Size = new System.Drawing.Size(68, 23);
            this.m_confirmPersonNumberButton.TabIndex = 5;
            this.m_confirmPersonNumberButton.Text = "确定人数";
            this.m_confirmPersonNumberButton.UseVisualStyleBackColor = true;
            this.m_confirmPersonNumberButton.Visible = false;
            this.m_confirmPersonNumberButton.Click += new System.EventHandler(this.OnConfirmPersonNumberButtonClick);
            // 
            // m_activityLabel
            // 
            this.m_activityLabel.AutoSize = true;
            this.m_activityLabel.Location = new System.Drawing.Point(312, 30);
            this.m_activityLabel.Name = "m_activityLabel";
            this.m_activityLabel.Size = new System.Drawing.Size(32, 17);
            this.m_activityLabel.TabIndex = 6;
            this.m_activityLabel.Text = "活动";
            this.m_activityLabel.Visible = false;
            // 
            // m_activityCountIntegerInput
            // 
            // 
            // 
            // 
            this.m_activityCountIntegerInput.BackgroundStyle.Class = "DateTimeInputBackground";
            this.m_activityCountIntegerInput.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.m_activityCountIntegerInput.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.m_activityCountIntegerInput.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left;
            this.m_activityCountIntegerInput.Location = new System.Drawing.Point(274, 64);
            this.m_activityCountIntegerInput.Name = "m_activityCountIntegerInput";
            this.m_activityCountIntegerInput.Size = new System.Drawing.Size(45, 23);
            this.m_activityCountIntegerInput.TabIndex = 7;
            this.m_activityCountIntegerInput.Visible = false;
            // 
            // m_confirmActivityCountButton
            // 
            this.m_confirmActivityCountButton.Location = new System.Drawing.Point(333, 64);
            this.m_confirmActivityCountButton.Name = "m_confirmActivityCountButton";
            this.m_confirmActivityCountButton.Size = new System.Drawing.Size(41, 23);
            this.m_confirmActivityCountButton.TabIndex = 8;
            this.m_confirmActivityCountButton.Text = "OK";
            this.m_confirmActivityCountButton.UseVisualStyleBackColor = true;
            this.m_confirmActivityCountButton.Visible = false;
            this.m_confirmActivityCountButton.Click += new System.EventHandler(this.OnConfirmActivityCountButtonClick);
            // 
            // m_resetActivityButton
            // 
            this.m_resetActivityButton.Location = new System.Drawing.Point(241, 351);
            this.m_resetActivityButton.Name = "m_resetActivityButton";
            this.m_resetActivityButton.Size = new System.Drawing.Size(68, 23);
            this.m_resetActivityButton.TabIndex = 4;
            this.m_resetActivityButton.Text = "重置活动";
            this.m_resetActivityButton.UseVisualStyleBackColor = true;
            this.m_resetActivityButton.Visible = false;
            this.m_resetActivityButton.Click += new System.EventHandler(this.OnResetActivityButtonClick);
            // 
            // m_confirmAllButton
            // 
            this.m_confirmAllButton.Location = new System.Drawing.Point(356, 351);
            this.m_confirmAllButton.Name = "m_confirmAllButton";
            this.m_confirmAllButton.Size = new System.Drawing.Size(68, 23);
            this.m_confirmAllButton.TabIndex = 5;
            this.m_confirmAllButton.Text = "确定";
            this.m_confirmAllButton.UseVisualStyleBackColor = true;
            this.m_confirmAllButton.Visible = false;
            this.m_confirmAllButton.Click += new System.EventHandler(this.OnConfirmAllButtonClick);
            // 
            // ConfirmActivityPremiseCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 386);
            this.Controls.Add(this.m_confirmActivityCountButton);
            this.Controls.Add(this.m_activityCountIntegerInput);
            this.Controls.Add(this.m_activityLabel);
            this.Controls.Add(this.m_confirmAllButton);
            this.Controls.Add(this.m_confirmPersonNumberButton);
            this.Controls.Add(this.m_resetActivityButton);
            this.Controls.Add(this.m_resetPersonButton);
            this.Controls.Add(this.m_activityPersonIntegerInput);
            this.Controls.Add(this.m_confirmActivityPersonCountButton);
            this.Controls.Add(this.m_activityActivityNameSplitter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_activityPersonNameSplitter);
            this.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ConfirmActivityPremiseCondition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfirmActivityPremiseCondition";
            ((System.ComponentModel.ISupportInitialize)(this.m_activityPersonIntegerInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_activityCountIntegerInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter m_activityPersonNameSplitter;
        private System.Windows.Forms.Splitter m_activityActivityNameSplitter;
        private System.Windows.Forms.Button m_confirmActivityPersonCountButton;
        private DevComponents.Editors.IntegerInput m_activityPersonIntegerInput;
        private System.Windows.Forms.Button m_resetPersonButton;
        private System.Windows.Forms.Button m_confirmPersonNumberButton;
        private System.Windows.Forms.Label m_activityLabel;
        private DevComponents.Editors.IntegerInput m_activityCountIntegerInput;
        private System.Windows.Forms.Button m_confirmActivityCountButton;
        private System.Windows.Forms.Button m_resetActivityButton;
        private System.Windows.Forms.Button m_confirmAllButton;
    }
}