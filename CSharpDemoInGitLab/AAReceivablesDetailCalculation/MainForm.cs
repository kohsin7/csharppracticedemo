﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AAReceivablesDetailCalculation
{
    public partial class MainForm : Form
    {
        private List<string> m_personNames;
        private List<string> m_activityNames;

        public MainForm()
        {
            InitializeComponent();
            InitializeFirstCondition();
        }

        private void InitializeFirstCondition()
        {
            try
            {
                ConfirmActivityPremiseCondition confirmForm = new ConfirmActivityPremiseCondition();
                if (confirmForm.ShowDialog() == DialogResult.OK)
                {
                    this.m_personNames = confirmForm.ActivityPersonNames;
                    this.m_activityNames = confirmForm.ActivityNames;
                }                
            }
            catch 
            {
                
            }
        }
    }
}
