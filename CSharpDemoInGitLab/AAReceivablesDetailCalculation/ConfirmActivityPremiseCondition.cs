﻿using DevComponents.Editors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AAReceivablesDetailCalculation
{
    public partial class ConfirmActivityPremiseCondition : Form
    {
        private List<TextBox> m_activityTextBoxs;
        private List<string> m_activityPersonNames = new List<string>();
        private List<string> m_activityNames = new List<string>();

        public List<string> ActivityPersonNames
        {
            get
            {
                return m_activityPersonNames;
            }
            set
            {
                m_activityPersonNames = value;
            }
        }
        public List<string> ActivityNames
        {
            get
            {
                return m_activityNames;
            }
            set
            {
                m_activityNames = value;
            }
        }
        public ConfirmActivityPremiseCondition()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {

        }

        private void InitializeActivityNameTextBox(object baseControl, int personCount)
        {
            m_activityTextBoxs = new List<TextBox>();
            (baseControl as Splitter).Controls.Clear();
            for (int i = 0; i < personCount; i++)
            {
                TextBox textBox = new TextBox();
                textBox.Location = new Point(45, (97 + i * 33));
                textBox.Size = new Size(101, 23);
                textBox.ImeMode = ImeMode.NoControl;
                m_activityTextBoxs.Add(textBox);
                (baseControl as Splitter).Controls.AddRange(m_activityTextBoxs.ToArray());
            }
        }

        /// <summary>
        /// 确认活动的参与人和活动项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnConfirmButtonClick(object sender, EventArgs e)
        {
            SetControlsVisible(false);
            InitializeActivityInformationsTextBox(m_activityPersonNameSplitter, m_activityPersonIntegerInput);
        }

        private void OnConfirmActivityCountButtonClick(object sender, EventArgs e)
        {
            this.m_activityCountIntegerInput.Enabled = false;
            this.m_confirmActivityCountButton.Enabled = false;
            this.m_resetActivityButton.Visible = true;
            this.m_confirmAllButton.Visible = true;
            InitializeActivityInformationsTextBox(m_activityActivityNameSplitter, m_activityCountIntegerInput);
        }

        private void InitializeActivityInformationsTextBox(object baseControl, object numberControl)
        {
            if (!string.IsNullOrEmpty((numberControl as IntegerInput).Text))
            {
                int personNumber = Convert.ToInt32((numberControl as IntegerInput).Text);
                InitializeActivityNameTextBox(baseControl, personNumber);
            }
        }

        private void OnResetPersonButtonClick(object sender, EventArgs e)
        {
            SetControlsVisible(true);
            this.m_activityPersonIntegerInput.Text = "";
            this.m_activityPersonNameSplitter.Controls.Clear();
        }

        private void SetControlsVisible(bool visible)
        {
            this.m_activityPersonIntegerInput.Enabled = visible;
            this.m_confirmActivityPersonCountButton.Enabled = visible;
            this.m_resetPersonButton.Visible = !visible;
            this.m_confirmPersonNumberButton.Visible = !visible;
        }

        private void OnConfirmPersonNumberButtonClick(object sender, EventArgs e)
        {
            m_activityPersonNames = CheckInput();
            if (m_activityPersonNames.Count() != m_activityPersonNames.Distinct().Count())
            {
                MessageBox.Show("相同名字请做区分处理", "提示", MessageBoxButtons.OK);
                m_activityPersonNames = new List<string>();
                return;
            }
            SetActivityControlsVisible(true);
        }

        private void SetActivityControlsVisible(bool visible)
        {
            this.m_activityCountIntegerInput.Visible = visible;
            this.m_activityLabel.Visible = visible;
            this.m_confirmActivityCountButton.Visible = visible;
        }

        private List<string> CheckInput()
        {
            var names = (from nameTextBoxs in m_activityTextBoxs
                         where !string.IsNullOrEmpty(nameTextBoxs.Text)
                         select nameTextBoxs.Text).ToList();

            return names;
        }

        private void OnResetActivityButtonClick(object sender, EventArgs e)
        {
            this.m_activityCountIntegerInput.Enabled = true;
            this.m_confirmActivityCountButton.Enabled = true;
            this.m_activityCountIntegerInput.Text = "";
            this.m_resetActivityButton.Visible = false;
            this.m_confirmAllButton.Visible = false;
            this.m_activityActivityNameSplitter.Controls.Clear();
        }

        private void OnConfirmAllButtonClick(object sender, EventArgs e)
        {
            m_activityNames = CheckInput();
            if (m_activityNames.Count() != m_activityNames.Distinct().Count())
            {
                MessageBox.Show("相同活动请做区分处理", "提示", MessageBoxButtons.OK);
                m_activityNames = new List<string>();
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();            
        }
    }
}
