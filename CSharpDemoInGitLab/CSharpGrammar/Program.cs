﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpGrammar
{
    /// <summary>
    /// 约束	s说明
    /// T：结构             类型参数必须是值类型
    /// T：类               类型参数必须是引用类型；这一点也适用于任何类、接口、委托或数组类型。
    /// T：new()            类型参数必须具有无参数的公共构造函数。 当与其他约束一起使用时，new() 约束必须最后指定。
    /// T：<基类名>	        类型参数必须是指定的基类或派生自指定的基类。
    /// T：<接口名称>	    类型参数必须是指定的接口或实现指定的接口。 可以指定多个接口约束。 约束接口也可以是泛型的。
    /// </summary>
    class Program
    {
        //泛型委托
        public delegate void SayHi<T>(T t);

        static void Main(string[] args)
        {
            //T 是 int类型
            GenericClass<int> generaicInt = new GenericClass<int>();
            generaicInt.m_T = 123;

            //T 是 String类型
            GenericClass<string> genericString = new GenericClass<string>();
            genericString.m_T = "123";

            //泛型缓存
            Show();
        }

        //注意：
        //基类约束时，基类不能是密封类，即不能是sealed类。sealed类表示该类不能被继承，在这里用作约束就无任何意义，因为sealed类没有子类。
        // 基类约束：约束T必须是People类型或者是People的子类
        public static void Show<T>(T tParameter) where T : People
        {
            Console.WriteLine($"{tParameter.Id}_{tParameter.Name}");
            tParameter.Hi();
        }

        public static T GetT<T>(T t) where T : ISports
        {
            t.PingPang();
            return t;
        }

        // 引用类型约束
        public static T GetClass<T>(T t) where T : class
        {
            return t;
        }

        // 值类型类型约束
        public static T GetStruct<T>(T t) where T : struct
        {
            return t;
        }

        // new()约束
        public static T GetNew<T>(T t) where T : new()
        {
            return t;
        }

        //多个约束new()放在最后一个
        public static void ShowMore<T>(T tParameter) where T : People, ISports, new()
        {

        }

        ICustomerListOut<Human> customerListOut1 = new CustomerListOut<People>();
        ICustomerListOut<Human> customerListOut2 = new CustomerListOut<Human>();

        ICustomerListIn<Human> customerListIn1 = new CustomerListIn<Human>();
        ICustomerListIn<People> customerListIn2 = new CustomerListIn<Human>();

        IMyList<People, Human> myList1 = new MyList<People, Human>();
        IMyList<People, Human> myList2 = new MyList<People, People>();//协变
        IMyList<People, Human> myList3 = new MyList<Human, Human>();//逆变
        IMyList<People, Human> myList4 = new MyList<Human, People>();//逆变+协变

        /// <summary>
        /// 泛型缓存
        /// </summary>
        public static void Show()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(GenericCache<int>.GetCache());
                Thread.Sleep(5000);
                Console.WriteLine(GenericCache<long>.GetCache());
                Thread.Sleep(5000);
                Console.WriteLine(GenericCache<DateTime>.GetCache());
                Thread.Sleep(5000);
                Console.WriteLine(GenericCache<string>.GetCache());
                Thread.Sleep(5000);
            }
        }
    }

    internal interface ISports
    {
        void PingPang();
    }

    public class Human
    {

    }

    public class People : Human
    {
        public object Id { get; set; }
        public object Name { get; set; }

        internal void Hi()
        {
            Console.WriteLine("Hi");
        }
    }

    /// <summary>
    /// 泛型类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericClass<T>
    {
        public T m_T;
    }

    /// <summary>
    /// 泛型接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericInterface<T>
    {
        //泛型类型的返回值
        T GetT(T t);
    }

    /// <summary>
    /// 使用泛型的时候必须指定具体类型，
    /// 这里的具体类型是int
    /// </summary>
    public class CommonClass : GenericClass<int>
    {

    }

    /// <summary>
    /// 子类也是泛型的，继承的时候可以不指定具体类型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommonClassChild<T> : GenericClass<T>
    {

    }

    /// <summary>
    /// 必须指定具体类型
    /// </summary>
    public class Common : IGenericInterface<string>
    {
        public string GetT(string t)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// 可以不知道具体类型，但是子类也必须是泛型的
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommonChild<T> : IGenericInterface<T>
    {
        public T GetT(T t)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// covariant
    /// 泛型接口的T前面有一个out关键字修饰，而且T只能是返回值类型，不能作为参数类型，
    /// 这就是协变 covariant。使用了协变以后，左边声明的是基类，右边可以声明基类或者基类的子类。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICustomerListOut<out T>
    {
        T GetT();
    }
    public class CustomerListOut<T> : ICustomerListOut<T>
    {
        public T GetT()
        {
            return default(T);
        }
    }

    /// <summary>
    /// 逆变 泛型接口的T前面有一个In关键字修饰，而且T只能方法参数，不能作为返回值类型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICustomerListIn<in T>
    {
        void ShowT(T t);
    }

    public class CustomerListIn<T> : ICustomerListIn<T>
    {
        public void ShowT(T t)
        {

        }
    }

    /// <summary>
    /// 协变和逆变同时使用
    /// </summary>
    /// <typeparam name="inT"></typeparam>
    /// <typeparam name="outT"></typeparam>
    public interface IMyList<in inT, out outT>
    {
        void Show(inT t);
        outT Get();
        outT Do(inT t);
    }

    public class MyList<T1, T2> : IMyList<T1, T2>
    {
        public void Show(T1 t)
        {
            Console.WriteLine(t.GetType().Name);
        }

        public T2 Get()
        {
            Console.WriteLine(typeof(T2).Name);
            return default(T2);
        }

        public T2 Do(T1 t)
        {
            Console.WriteLine(t.GetType().Name);
            Console.WriteLine(typeof(T2).Name);
            return default(T2);
        }
    }

    /// <summary>
    /// 泛型缓存
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericCache<T>
    {
        private static string m_typeTime = "";

        public static string GetCache()
        {
            return m_typeTime;
        }

        static GenericCache()
        {
            Console.WriteLine("This is GenericCache static method");
            m_typeTime = string.Format("{0}_{1}", typeof(T).FullName, DateTime.Now.ToString(""));
        }
    }
}